<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SlimQuickGarcinia</title>
<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="assets/fonts/fonts.css"/>
</head>
<body class="checkout">
    <div class="page">
        <div id="wrapper" class="wrapper clearfix">
        	<div id="header">
            	<h1 id="logo" class="left"><a href="#" title="Logo">Logo</a></h1>
                <h1 class="right title">CONGRATULATIONS!<br />AVAILABLE IN YOUR AREA!</h1>
            </div>
            <div class="container" id="main">
            	<div class="content orderSummary">
                    <ul class="row2 clearfix boxes cf">
                     	<li class="col_tilte">
                        	<div class="col_1">This is your last chance! 1 Month with FREE Shipping.</div>
                            <div class="col_2">Price</div>
                        </li>
                        <li>
                        	<div class="col_1">
                            	<div class="checkbox"></div>
                            	<div class="product"><img src="assets/img/bottles21.jpg" alt="" /></div>
                                <div class="product_dec" align="center"><strong>Last Chance:</strong><br />Lose up to 10 lbs and 1-2 inches of belly fat!</div>
                            </div>
                            <div class="col_2">
                            	<div class="price"><strong>$39.99 </strong><br />per bottle</div>
                                <button class="sel_btn" type="submit" name="Select" value="Select"><strong>SELECT</strong></button>
                            </div>
                        </li>
                    </ul>
                    <ul class="row3 clearfix cf">
                     	<li class="col_tilte">
                        	<div class="col_1">Product</div>
                            <div class="col_3">Quantity</div>
                            <div class="col_2">Price</div>
                        </li>
                        <li>
                        	<div class="col_1">
                                <h3>Cambogia Trim Advanced</h3>
                            	<div class="product"><img src="assets/img/ups.png" alt="" /></div>
                                <div class="product_dec">Your order will be shipped via UPS Innovations and should arrive within 3-7 business days.</div>
                            </div>
                            <div class="col_3"><strong> 1 </strong></div>
                            <div class="col_2"><strong> $39.00 </strong></div>
                        </li>
                        <li class="summary">
                        	<div class="sub_total">Sub-Total<span class="sub_price">$39.00</span></div>
                            <div class="sub_total">Shipping and Handling<span class="sub_price">$0.00</span></div>
                            <div class="sub_total">Total<span class="sub_price">$39.00</span></div>
                        </li>
                    </ul>
                </div>
                <div class="right_area index">
                	<div class="title">
                    	<h2 class="title">PAYMENT INFORMATION</h2>
                    	<h1 class="title">FINAL STEP</h1>
                    </div>
                	<div class="module">
                    <div class="payment"><img src="assets/img/payment.png" alt="" /></div>
               		   <form class="form"  action="" method="post" id="checkoutForm">
                            <p class="fields">
                                <input class="input" placeholder="First Name" type="text" name="first_name" />
                            </p>
                            <p class="fields">
                                <input class="input" placeholder="Last Name" type="text" name="last_name" />
                            </p>
                            <p class="fields">
                                <input class="input" placeholder="Address" type="text" name="address" />
                            </p>
                            <p class="fields">
                                <input class="input" placeholder="City" type="text" name="city" />
                            </p>
                            <p class="fields">
                                <select name="state" id="id_state" class="">
                                    <option value="">State</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DC">District of Columbia</option>
                                    <option value="DE">Delaware</option>
                                    <option value="FL">Florida</option>
                                    <option value="FM">Federated States of Micronesia</option>
                                    <option value="GA">Georgia</option>
                                    <option value="GU">Guam</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="IA">Iowa</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MD">Maryland</option>
                                    <option value="ME">Maine</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MT">Montana</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NY">New York</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="PW">Palau</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VA">Virginia</option>
                                    <option value="VI">Virgin Islands</option>
                                    <option value="VT">Vermont</option>
                                    <option value="WA">Washington</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WY">Wyoming</option>
                                    <option value="AE">Armed Forces Africa</option>
                                    <option value="AA">Armed Forces Americas (except Canada)</option>
                                    <option value="AE">Armed Forces Canada</option>
                                    <option value="AE">Armed Forces Europe</option>
                                    <option value="AE">Armed Forces Middle East</option>
                                    <option value="AP">Armed Forces Pacific</option>
                                </select>
                            </p>
                            <p class="fields">
                                <input class="input small-input" placeholder="Zip" type="text" name="zip" maxlength="5" />
                            </p>
                            <p class="fields">
                                <input class="input" placeholder="Phone Number" type="tel" name="phone" maxlength="10" />
                            </p>
                            <p class="fields">
                                <input class="input" placeholder="Email" type="email" name="email_address" />
                            </p>
                            <p class="fields"><input class="input" type="text" name="creditcard" placeholder="Card Number"></p>
                            <p class="cf select_field fields">
                                <select name="exp_mon" class="expiration small-input">
                                    <option selected="selected" value="">Expiry Month</option>
                                    <option value="01">Jan (01)</option>
                                    <option value="02">Feb (02)</option>
                                    <option value="03">Mar (03)</option>
                                    <option value="04">Apr (04)</option>
                                    <option value="05">May (05)</option>
                                    <option value="06">Jun (06)</option>
                                    <option value="07">Jul (07)</option>
                                    <option value="08">Aug (08)</option>
                                    <option value="09">Sep (09)</option>
                                    <option value="10">Oct (10)</option>
                                    <option value="11">Nov (11)</option>
                                    <option value="12">Dec (12)</option>
                                </select>
                                <select name="exp_year" class="expiration">
                                    <option value="" selected="selected">Expiry Year</option>
                                    <option value="13">2013</option>
                                    <option value="14">2014</option>
                                    <option value="15">2015</option>
                                    <option value="16">2016</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                    <option value="25">2025</option>
                                    <option value="26">2026</option>
                                    <option value="27">2027</option>
                                    <option value="28">2028</option>
                                    <option value="29">2029</option>
                                    <option value="30">2030</option>
                                </select>
                            </p>
                            <p class="cf fields">
                                <input class="input small-input" type="text" name="cvv" placeholder="CVV" maxlength="4">
                                <a class="cvvpop" href="#" title="">what's this</a>
                            </p>
                   
                    		<p class="option_billing clearfix">
                              <input type="checkbox" id="c1" name="agree_option" />
                              <label class="terms" for="c1">By submitting my information above I agree
to the <a data-target="#myModal" data-toggle="modal" href="#" class="check1">Terms & Conditions</a> and <a href="#">Privacy Policy</a>
of this offer.</label>
                              
                            </p>
                            <p>
                            	<input class="submit button rush" type="submit" name="submit" value="RUSH MY ORDER" />
                            </p>
                        </form> 
                </div>
                <div class="module border_none">
                	<a title="Lock" href="#" class="lock">This is a 128-Bit Secure SSL Connection</a>
                    <a title="" href="#"><img alt="" src="assets/img/trust_MacAfee.jpg"></a>
                	
                </div>
                </div>
            </div>
        </div>
    </div><!--/Paage-->
        <div class="footer">
        <p><a href="#">terms & Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">Privacy Policy</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">Contact Us</a></p>
        <p>{corp}}</p><p>{{tfn}}</p>
        </div>
    <div class="modal hide fade" id="myModal">
                <div class="modal-body">
                <button id="close" class="close" data-dismiss="modal" type="button"></button>
                <h1 class="modal_logo"><img src="assets/img/logo.png" align="" /></h1>
                    <div class="popup-content">
                    	<h2>Cras mattis consectetur purus sit amet</h2> 

<p>fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>

<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec ullamcorper nulla non metus auctor fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mattis consectetur purus sit amet fermentum. Donec ullamcorper nulla non metus auctor fringilla. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

<p>Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Curabitur blandit tempus porttitor.</p>

<h2>Cras mattis consectetur purus</h2>

<p>sit amet fermentum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>

<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec ullamcorper nulla non metus auctor fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mattis consectetur purus sit amet fermentum. Donec ullamcorper nulla non metus auctor fringilla. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

<p>Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Curabitur blandit tempus porttitor.</p>
                    </div>
                </div>
            </div>
	<script src="assets/js/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/jquery.placeholder.min.js"></script>
</body>
</html>
