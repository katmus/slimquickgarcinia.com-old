<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SlimQuickGarcinia</title>
<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="assets/fonts/fonts.css"/>
<script src="assets/js/jquery.min.js"></script>
<script>
			function equalHeight(group) {
			tallest = 0;
			group.each(function() {
			thisHeight = $(this).height();
			if(thisHeight > tallest) {
			tallest = thisHeight;
			}
			});
			group.height(tallest);
			}
			$(document).ready(function() {
			equalHeight($(".row"));
			});
</script>
</head>
<body class="checkout">
    <div class="page">
        <div id="wrapper" class="wrapper clearfix">
        	<div id="header">
            	<h1 id="logo" class="left"><a href="#" title="Logo">Logo</a></h1>
                <h1 class="right title">THANK YOU<br />FOR YOUR PURCHASE!</h1>
            </div>
            <div class="container" id="main">
            	<div class="confirm">
                    <ul class="colum colum_1 clearfix active">
                     	<li class="col_tilte clearfix">
                        	<div class="row_1"><h2 align="center">Summary of Purchase</h2></div>
                        </li>
                        <li class="clearfix">
                        	<div class="row_1">
                            	<div class="guaranty2"><img src="assets/img/guaranteed2.png" alt="" /></div>
                                <p style="margin-top:15px;">Congratulations your order has been confirmed. An order notification has been sent to your email address listed below. If this information is incorrect please contact us at: <strong> {TFN}.</strong> Be sure to <a href="javascript:window.print()">print this page</a> for your records.</p>
                            </div>
                        </li>
                    </ul>
                    <ul class="colum colum_3 clearfix">
                     	<li class="col_tilte clearfix">
                        	<div class="row_1">Product Details</div>
                            <div class="row_2">Price</div>
                             <div class="row_3">Date of Purchase</div>
                        </li>
                        <li class="clearfix">
                        	<div class="row_1">
                            	<p><strong>{product_name}</strong></p>
                            </div>
                        	<div class="row_2">
                                <p><strong>Shipping Cost:</strong>${cost}</p>
                                <p><strong>Sales Tax:</strong>${tax}</p>
                                <p><strong>Total:</strong>${total}</p>
                            </div>
                            <div class="row_3">
                            	<p><strong>{Date}</strong></p>
                            </div>
                        </li>
                    </ul>
                    <ul class="colum colum_2 clearfix">
                     	<li class="col_tilte clearfix">
                        	<div class="row_1">Billing Information</div>
                            <div class="row_2">Shipping Information</div>
                        </li>
                        <li class="clearfix">
                            <div class="row_1">
                            	<p><strong>Name:</strong>{first_last_name}</p>
                                <p class="clearfix">
                                <strong>Address:</strong>
                                <span>{user_address_1}</span>
                                <span>{user_address_2}</span>
                                <span>{user_city} , {user_state}</span>
                                <span>{user_zipcode</span></p>
                                <p><strong>Phone:</strong>{user_phone}</p>
                                <p><strong>Email:</strong>{user_email}</p>
                            </div>
                            <div class="row_2">
                            	<p class="clearfix"><strong>Name:</strong>{first_last_name}</p>
                                <p class="clearfix">
                                <strong>Address:</strong>
                                <span>{user_address_1}</span>
                                <span>{user_address_2}</span>
                                <span>{user_city},{user_state}</span>
                                <span>{user_zipcode</span></p>
                            </div>
                        </li>
                     	<li class="col_tilte clearfix">
                        	<div class="row_1">Credit Card Used</div>
                            <div class="row_2">The charge on your card will appear as:</div>
                        </li>
                        <li class="clearfix">
                            <div class="row row_1 credit_card"><img src="assets/img/mastercard.png" alt=""><p><strong>Card Type: Mastercard</strong></p><p><strong>Card Num: ************2012</strong></p></div>
                            <div class="row row_2" align="center">
                            	<h3 align="center">CREDIT CARD STATEMENT</h3>
                                <form method="get">
                                	<input id="cost" type="text" value="${cost}">
                                </form>
                                <img src="assets/img/cardinfo.png" alt="">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer">
        <p><a href="#">terms & Conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">Privacy Policy</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">Contact Us</a></p>
        <p>{corp}}</p><p>{{tfn}}</p>
        </div>
        </div>
    </div><!--/Paage-->
    <script>window.jQuery || document.write('<script src="/js/jquery.min.js"><\/script>')</script>
    <script src="/js/app.min.js"></script>
    <script src="/js/jquery.placeholder.min.js"></script>
</body>
</html>
